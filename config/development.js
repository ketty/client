module.exports = {
  export: {
    rootFolder: 'config/exportScripts',
    scripts: [],
  },
  templates: [
    {
      label: 'atla',
      url: 'https://gitlab.coko.foundation/ketida-templates/atla',
      assetsRoot: 'dist',
      supportedNoteTypes: ['footnotes'],
    },
    {
      label: 'lategrey',
      url: 'https://gitlab.coko.foundation/ketida-templates/lategrey',
      assetsRoot: 'dist',
      supportedNoteTypes: ['footnotes'],
    },
    {
      label: 'significa',
      url: 'https://gitlab.coko.foundation/ketida-templates/significa',
      assetsRoot: 'dist',
      supportedNoteTypes: ['footnotes'],
    },
    {
      label: 'bikini',
      url: 'https://gitlab.coko.foundation/ketida-templates/bikini',
      assetsRoot: 'dist',
      supportedNoteTypes: ['footnotes'],
    },
  ],
}
